﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player;
    public GameEnding gameEnding;

    AudioSource m_AudioSource;
    bool m_IsPlayerInRange;

    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }

        /*
         * When the player scares a ghost, they turn red, play a scream, and run away to a designated "hidepoint". 
         * There aren't any gradual effects like there are for the game ending, so this doesn't have to be in Update().
         * This also allows me to use the 'other' collider to find the ghosts' transforms.       
         * By tagging all 4 ghosts as "Ghost", this raycast works on all of them, and would work on any future ones.
         * To do this the way it was done for the player in the tutorial, I'd have to make a public field for all 4 ghosts.
         * Extra "and" condition keeps ghosts from screaming multiple times.    
         */       
        if(CanSee(other.transform.position) == other.CompareTag("Ghost") && other.GetComponentInParent<WaypointPatrol>().scared == false)
        {
            m_AudioSource.Play();
            SkinnedMeshRenderer set = other.GetComponentInChildren<SkinnedMeshRenderer>();
            set.material = set.materials[1];
            //Affecting the 'scared' boolean in WaypointPatrol to fix pathing to the "safe spot"
            other.GetComponentInParent<WaypointPatrol>().scared = true; 
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if(m_IsPlayerInRange)
        {
            if(CanSee(player.position) == player.CompareTag("Player"))
            { 
                //extra "and" condition makes it so the player can't be caught by frightened (red) ghosts.
                if (transform.parent.CompareTag("Ghost") && GetComponentInParent<WaypointPatrol>().scared == false)
                {
                    gameEnding.CaughtPlayer();
                }
                else if(transform.parent.CompareTag("Gargoyle"))
                {
                    gameEnding.CaughtPlayer();
                }

            }
        }
    }

    /*
     * Because the Raycasting code to catch the player and scare the ghosts was
     * nearly identical, it was put into its own function.
     */
   
    private Transform CanSee(Vector3 position)
    {
        Vector3 direction = position - transform.position - Vector3.up;
        Ray ray = new Ray(transform.position, direction);
        RaycastHit raycastHit;
        Physics.Raycast(ray, out raycastHit);
        return raycastHit.transform;
    }
}
