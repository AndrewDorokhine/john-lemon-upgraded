﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    [SerializeField] Transform hidepoint; //send ghosts to shelter with a gargoyle if scared

    public CharSpeeds speed; //my ScriptableObject for player speed

    private int m_CurrentWaypointIndex;
    private float timer;

    private SkinnedMeshRenderer set;
    private Material origMat;

    public bool scared; //Needs to be public because Observer.cs talks to it.

    void Start()
    {
        set = this.GetComponentInChildren<SkinnedMeshRenderer>();
        //required to restore white shader rim when fear goes away. Otherwise the red one replaces it in the materials array when it changes.
        origMat = set.materials[0];

        navMeshAgent.SetDestination(waypoints[0].position);
        scared = false;
    }

    void Update()
    {
        navMeshAgent.speed = speed.getMoveSpeed();
        //Activates timer and multiplies speed by 4 for running away
        if (scared)
        {
            navMeshAgent.speed = speed.getMoveSpeed() * 4;
            navMeshAgent.SetDestination(hidepoint.position);
            timer += Time.deltaTime;

            //Once the timer ends, it resets to 0 and the ghost can go back to its patrol.
            if (timer >= 10.0f)
            {
                scared = false;
                timer = 0f;
                navMeshAgent.speed = speed.getMoveSpeed();
                set.material = origMat;
            }
        }

        else if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
