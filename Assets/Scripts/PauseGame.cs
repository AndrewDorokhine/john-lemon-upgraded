﻿using UnityEngine;

public class PauseGame : MonoBehaviour
{
    private bool paused = false;
    [SerializeField] Transform menu;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            paused = !paused;
            Time.timeScale = paused ? 0 : 1;
            menu.gameObject.SetActive(paused);
        }
    }

    private void SetTime(float time)
    {
        Time.timeScale = time; //Short enough to not need its own function
    }
}
