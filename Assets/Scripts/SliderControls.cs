﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SliderControls : MonoBehaviour
{
    public AudioMixer mix;
    [SerializeField] private string _volumeControlName;
    [SerializeField] Slider slider;

    void Start()
    {
        mix.GetFloat(_volumeControlName, out float curVol);
        slider.value = curVol;
    }

    public void SetVolume(float value)
    {
        mix.SetFloat(_volumeControlName, value);
    }
}
