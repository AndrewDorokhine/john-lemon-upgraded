﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SpeedData", menuName = "SpeedData")]
public class CharSpeeds : ScriptableObject
{
    [SerializeField] float moveSpeed;

    public float getMoveSpeed()
    {
        return moveSpeed;
    }

}
